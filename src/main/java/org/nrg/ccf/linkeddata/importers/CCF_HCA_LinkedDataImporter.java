package org.nrg.ccf.linkeddata.importers;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.linkeddata.AbstractLinkedDataImporter;
import org.nrg.ccf.linkeddata.LinkedFileInfo;
import org.nrg.ccf.linkeddata.util.LinkedDataUtil;
import org.nrg.ccf.linkeddata.util.comparators.FileNameComparator;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;

/**
 * The Class CCF_HCA_LinkedDataImporter.
 */
public class CCF_HCA_LinkedDataImporter extends AbstractLinkedDataImporter {

	/** The scan list. */
	final List<XnatImagescandataI> scanList = new ArrayList<>();
	
	/** The upload file map. */
	final Map<File, LinkedFileInfo> uploadFileMap = new HashMap<>(); 
	
	/** The psychopy folder. */
	String PSYCHOPY_FOLDER = "PSYCHOPY";
	
	/** The scan psychopy types. */
	String[] SCAN_PSYCHOPY_TYPES = { "mbPCASL", "REST", "CARIT", "FACENAME", "VISMOTOR" };
	
	/** CCF-558:  Some FACENAME csv files will be stored at the session level rather than scan level.  */
	String[] SPECIAL_HANDLING_TYPES = { "FACENAME" };
	
	/** The scan psychopy ext. */
	String[] SCAN_PSYCHOPY_EXT = { "csv", "mp4" };
	
	/** The scan psychopy scantypes. */
	String[] SCAN_PSYCHOPY_SCANTYPES = { "mbPCASL", "rfMRI", "tfMRI" };
	
	/** The session psychopy folder. */
	String SESSION_PSYCHOPY_FOLDER = "PSYCHOPY";
	
	/** The session psychopy types. */
	String[] SESSION_PSYCHOPY_TYPES = { "mbPCASL", "REST", "CARIT", "FACENAME", "VISMOTOR" };
	
	/** The session psychopy ext. */
	String[] SESSION_PSYCHOPY_EXT = { "csv", "psydat", "log", "mp4" };

	/**
	 * Instantiates a new CC f_ hc a_ linked data importer.
	 *
	 * @param listenerControl the listener control
	 * @param u the u
	 * @param fw the fw
	 * @param params the params
	 */
	public CCF_HCA_LinkedDataImporter(Object listenerControl, UserI u, FileWriterWrapperI fw, Map<String, Object> params) {
		super(listenerControl, u, fw, params);
	}
	
	/**
	 * Instantiates a new CC f_ hc a_ linked data importer.
	 *
	 * @param u the u
	 * @param params the params
	 */
	public CCF_HCA_LinkedDataImporter(UserI u, Map<String, Object> params) {
		super(u, params);
	}

	/* (non-Javadoc)
	 * @see org.nrg.ccf.linkeddata.AbstractLinkedDataImporter#processCacheFiles(java.io.File)
	 */
	@Override
	public void processCacheFiles(File cacheLoc) throws ClientException, ServerException {
		
		final XnatMrsessiondata exp = (this.getCurrentExperiment() instanceof XnatMrsessiondata) ? (XnatMrsessiondata)this.getCurrentExperiment() : null;
		final List<String> returnList = this.getReturnList();
		
		returnList.add("<b>BEGIN PROCESSING UPLOADED FILES (EXPERIMENT = " + exp.getLabel() + ")</b>");
		scanList.addAll(exp.getScans_scan());
		LinkedDataUtil.sortTheScanList(scanList,getReturnList());
		for (final File f : FileUtils.listFiles(cacheLoc, FileFileFilter.FILE, TrueFileFilter.TRUE)) {
			uploadFileMap.put(f, null);
		}
		
		// Start matching files to scans
		returnList.add("<br><b>PROCESS SCAN-LEVEL PSYCHOPY FILES</b>");
		processScanLevelPsychopyFiles();
		
		// Start matching files to session
		returnList.add("<br><b>PROCESS SESSION-LEVEL FILES</b>");
		processSessionLevelPsychopyFiles();
		
		returnList.add("<br><b>REPORT ON LEFTOVER FILES</b>");
		reportLeftoverFiles();
		
		Boolean upload_status=uploadFilesToResources(uploadFileMap);
		
		LinkedDataUtil.addUploadStatus(upload_status, returnList);
		
		returnList.add("<br><b>LINKED DATA UPLOAD COMPLETE (EXPERIMENT = " + exp.getLabel() + ")</b>");
		
	}


	/**
	 * Report leftover files.
	 */
	private void reportLeftoverFiles() {
		final List<File> keyList = new ArrayList<>();
		keyList.addAll(uploadFileMap.keySet());
		Collections.sort(keyList, new FileNameComparator());
		for (final File f : keyList) {
			if (uploadFileMap.get(f) == null) {
				getReturnList().add("WARNING:  File - <b>" + f.getName() + "</b> - was not uploaded to any scan or to the session.");
			}
		}
	}

	/**
	 * Process scan level psychopy files.
	 *
	 * @throws ClientException the client exception
	 */
	private void processScanLevelPsychopyFiles() throws ClientException {
		final Map<String,Map<Integer,List<File>>> psychopyFileMap = buildPsychopyFileMap();
		final Map<String,List<XnatImagescandataI>> usableScanMap = buildEmptyPsychopyScanMap();
		final Map<String,List<XnatImagescandataI>> fullScanMap = buildEmptyPsychopyScanMap();
		// Populate psychopy map scan lists 
		for (XnatImagescandataI scan : scanList) {
			final String[] sdParts = scan.getSeriesDescription().split("_");
			// NOTE:  mbPCASL is handled differently (different series description pattern)
			final String fmriType = (sdParts.length>1) ? ((sdParts[0].startsWith("mbPCASL")) ? "mbPCASL" : sdParts[1]) : "NOT_FMRI";
			if (!Arrays.asList(SCAN_PSYCHOPY_TYPES).contains(fmriType)) {
				continue;
			}
			if (!Arrays.asList(SCAN_PSYCHOPY_SCANTYPES).contains((scan.getType().equals("mbPCASLhr")) ? "mbPCASL" : scan.getType())) {
				continue;
			}
			final List<XnatImagescandataI> fullScanList = fullScanMap.get(fmriType);
			fullScanList.add(scan);
			if (!scan.getQuality().toLowerCase().contains("unusable")) {
				final List<XnatImagescandataI> usableScanList = usableScanMap.get(fmriType);
				usableScanList.add(scan);
			}
		}
		// Now decide which lists go with which scans
		for (final String fmriType : psychopyFileMap.keySet()) {
			final Map<Integer, List<File>> psFileMap = psychopyFileMap.get(fmriType);
			final List<XnatImagescandataI> usableScanList = usableScanMap.get(fmriType);
			final List<XnatImagescandataI> fullScanList = fullScanMap.get(fmriType);
			final Set<Integer> psFileMapKeys = psFileMap.keySet();
			final List<Integer> fileRuns = new ArrayList<>();
			fileRuns.addAll(psFileMapKeys);
			Collections.sort(fileRuns);
			if (fileRuns.size() == fullScanList.size()) {
				equalSizeUpdate(psFileMap, fileRuns, fullScanList);
			} else if (fileRuns.size() == usableScanList.size()) {
				equalSizeUpdate(psFileMap, fileRuns, usableScanList);
			} else if (fileRuns.size() < usableScanList.size()) {
				// Just try to match run number (N) to the Nth usable scan 
				for (int i=0; i<fileRuns.size(); i++) {
					if (fileRuns.size()<i) {
						continue;
					}
					final Integer fileRun = fileRuns.get(i);
					final XnatImagescandataI runScan =  usableScanList.get(fileRun-1);
					if (runScan==null) {
						continue;
					}
					final List<File> runFileList = psFileMap.get(fileRun);
					if (runFileList == null) {
						continue;
					}
					for (final File runFile : runFileList) {
						uploadFileMap.put(runFile, new LinkedFileInfo(runScan, PSYCHOPY_FOLDER));
					}
				}
			} else if (fileRuns.size() > fullScanList.size()) {
				throw new ClientException(
						"&nbsp;<b>ERROR:  Too many psychopy file run values for number of scans collected in this session (SCANTYPE=" + fmriType +  ").</b>" +
						"<ul style='margin-left:0px;width:90%;'>" + 
							"<li style='margin-bottom:5px'>It appears there are files from more scan runs than there are scans.</li>" +
							"<li style='margin-bottom:5px'>Note that run values are constructed not just from the run number indicated in the filename " +
								"but from date and time information.</li>" +
							"<li>This error can also occur when you have duplicate files indicated with with a copy indicator " +
								"(e.g. \"psychopyFileName_run1_2017-01-01-000000 (1).csv\"), as the copy indicator can be seen as part " +
								"of the datetime information if it appears in that part of the filename.</li>" +
						"</ul>"
				);
			}
		}
	}

	/**
	 * Process session level psychopy files.
	 *
	 * @throws ClientException the client exception
	 */
	private void processSessionLevelPsychopyFiles() throws ClientException {
		for (final File f : uploadFileMap.keySet()) {
			final String fileName = f.getName();
			if (uploadFileMap.get(f) != null) {
				// File already assigned to scan.
				continue;
			}
			final String[] fileNameParts = fileName.split("\\.(?=[^\\.]+$)");
			final String baseName = fileNameParts[0];
			final String fileExt = (fileNameParts.length>1) ? fileNameParts[fileNameParts.length-1] : ""; 
			final String[] baseNameParts = baseName.split("_");
			// Has right extension?
			if (!Arrays.asList(SESSION_PSYCHOPY_EXT).contains(fileExt)) {
				continue;
			}
			// Begins with fMRI type?
			final String psychopyType = baseNameParts[0];
			if (!Arrays.asList(SESSION_PSYCHOPY_TYPES).contains(psychopyType) && 
					// Per Greg 2014, want to catch files like {EXPLABEL}_{DATEINFO}.{PSYCHOPY_EXT} 
					!f.getName().startsWith(getCurrentExperiment().getLabel())) {
				continue;
			}
			// Contains experiment label?
			if (!f.getName().contains(getCurrentExperiment().getLabel())) {
				throw new ClientException(
						"&nbsp;<b>ERROR:  One or more psychopy files does not contain the Session ID that is expected for these scans.  (SESSIONID=" +
								getCurrentExperiment().getLabel() + ", FILENAME=" + fileName + ").</b>" +
						"<ul style='margin-left:0px;width:90%;'><li style='margin-bottom:5px'>This may have occurred due to uploading files from a different session.</li>" +
						"<li style='margin-bottom:5px'>This may have occurred due to misnaming files in the correct session.</li>" +
						"<li>Please check all files, as this check stops processing at the first invalid file found.</li></ul>");
			}
			uploadFileMap.put(f, new LinkedFileInfo(null, SESSION_PSYCHOPY_FOLDER));
		}
	}

	/**
	 * Equal size update.
	 *
	 * @param psFileMap the ps file map
	 * @param fileRuns the file runs
	 * @param cScanList the c scan list
	 */
	private void equalSizeUpdate(Map<Integer, List<File>> psFileMap, List<Integer> fileRuns, List<XnatImagescandataI> cScanList) {
		LinkedDataUtil.sortTheScanList(cScanList, getReturnList());
		for (int i=0; i<fileRuns.size(); i++) {
			final Integer fileRun = fileRuns.get(i);
			final List<File> runFileList = psFileMap.get(fileRun);
			if (runFileList==null) {
				continue;
			}
			for (final File runFile : runFileList) {
				uploadFileMap.put(runFile, new LinkedFileInfo(cScanList.get(i), PSYCHOPY_FOLDER));
			}
		}
	}

	/**
	 * Builds the empty psychopy scan map.
	 *
	 * @return the map
	 */
	private Map<String, List<XnatImagescandataI>> buildEmptyPsychopyScanMap() {
		final Map<String,List<XnatImagescandataI>> initScanMap = new HashMap<>();
		for (String type : SCAN_PSYCHOPY_TYPES) {
			initScanMap.put(type, new ArrayList<XnatImagescandataI>());
		}
		return initScanMap;
	}

	/**
	 * Builds the psychopy file map.
	 *
	 * @return the map
	 * @throws ClientException the client exception
	 */
	private Map<String, Map<Integer, List<File>>> buildPsychopyFileMap() throws ClientException {
		final Map<String,Map<String,List<File>>> psychopyInitFileMap = new HashMap<>();
		final Map<String,Map<Integer,List<File>>> psychopyFinalFileMap = new HashMap<>();
		final Map<String,List<String>> runSortInfo = new HashMap<>();
		for (final File f : uploadFileMap.keySet()) {
			final String fileName = f.getName();
			if (uploadFileMap.get(f) != null) {
				// File already assigned to scan.
				continue;
			}
			final String[] fileNameParts = fileName.split("\\.(?=[^\\.]+$)");
			final String baseName = fileNameParts[0];
			final String fileExt = (fileNameParts.length>1) ? fileNameParts[fileNameParts.length-1] : ""; 
			final String[] baseNameParts = baseName.split("_");
			// Has right extension?
			if (!Arrays.asList(SCAN_PSYCHOPY_EXT).contains(fileExt)) {
				continue;
			}
			// Begins with fMRI type?
			final String psychopyType = baseNameParts[0];
			if (Arrays.asList(SPECIAL_HANDLING_TYPES).contains(psychopyType)) {
				// We want these to go to the site level.  
				if (fileName.matches("^.*_run[1-9]_.*$") && fileName.toLowerCase().endsWith(".csv") &&
						!fileName.toLowerCase().contains("_wide")) {
					continue;
				}
			}
			if (!Arrays.asList(SCAN_PSYCHOPY_TYPES).contains(psychopyType)) {
				continue;
			}
			final StringBuilder expSb = new StringBuilder();
			int specRunNumber = -1;
			String datePart = "";
			String timePart = "";
			String sortInfo = "";
			for (int i=0; i<baseNameParts.length; i++) {
				if (baseNameParts[i].matches("^run[1-9]$")) {
					specRunNumber = Integer.valueOf(baseNameParts[i].substring("run".length()));
					if (i<baseNameParts.length-1) {
						datePart = baseNameParts[i+1];
					}
					if (i<baseNameParts.length-2) {
						timePart = baseNameParts[i+2];
					}
					if (!runSortInfo.containsKey(psychopyType)) {
						runSortInfo.put(psychopyType, new ArrayList<String>());
					}
					sortInfo = datePart + "_" + timePart + "_" + specRunNumber;
					final List<String> infoList = runSortInfo.get(psychopyType);
					if (!infoList.contains(sortInfo)) {
						infoList.add(sortInfo);
					}
					Collections.sort(infoList);
					break;
				} else if (i>0) {
					expSb.append(baseNameParts[i]);
					if (i<(baseNameParts.length-1) && !baseNameParts[i+1].matches("^run[1-9]$")) {
						expSb.append("_");
					}
				}
			}
			final String expFromFileName = expSb.toString();
			// Has run number (note:  run0 is a practice type and not included here)?
			if (specRunNumber<1) {
				continue;
			}
			// Contains experiment label?
			if (!expFromFileName.equals(getCurrentExperiment().getLabel())) {
				if (expFromFileName.length() == getCurrentExperiment().getLabel().length()) {
					throw new ClientException(
							"&nbsp;<b>ERROR:  One or more psychopy files does not contain the Session ID that is expected for these scans.  (SESSIONID=" +
									getCurrentExperiment().getLabel() + ", FILENAME=" + fileName + ").</b>" +
							"<ul style='margin-left:0px;width:90%;'><li style='margin-bottom:5px'>This may have occurred due to uploading files from a different session.</li>" +
							"<li style='margin-bottom:5px'>This may have occurred due to misnaming files in the correct session.</li>" +
							"<li>Please check all files, as this check stops processing at the first invalid file found.</li></ul>");
				}
				continue;
			}
			// We have a psychopy file.  Place it in the list.
			if (!psychopyInitFileMap.containsKey(psychopyType)) {
				psychopyInitFileMap.put(psychopyType, new HashMap<String,List<File>>());
			}
			final Map<String, List<File>> runMap = psychopyInitFileMap.get(psychopyType);
			if (!runMap.containsKey(sortInfo)) {
				runMap.put(sortInfo, new ArrayList<File>());
			}
			final List<File> runFileList = runMap.get(sortInfo);
			runFileList.add(f);
		}
		// NOTE:  We can't use the run number from the file name.  There may be multiple values for the same run number.  We'll calculate a 
		// run number from the ordered list of files using the sortInfo field calculated and stored
		for (final Entry<String, Map<String, List<File>>> entry : psychopyInitFileMap.entrySet()) {
			final String psychopyType = entry.getKey();
			final Map<String, List<File>> psychopyMap = entry.getValue();
			for (final Entry<String, List<File>> psychopyEntry : psychopyMap.entrySet()) {
				final String sortInfo = psychopyEntry.getKey();
				final List<File> fileList = psychopyEntry.getValue();
				final Integer calcRunNumber = getRunNumberFromSortInfo(runSortInfo,psychopyType,sortInfo);
				if (!psychopyFinalFileMap.containsKey(psychopyType)) {
					psychopyFinalFileMap.put(psychopyType, new HashMap<Integer,List<File>>());
				}
				final Map<Integer, List<File>> finalMap = psychopyFinalFileMap.get(psychopyType);
				if (!finalMap.containsKey(calcRunNumber)) {
					finalMap.put(calcRunNumber, new ArrayList<File>());
				}
				final List<File> finalList = finalMap.get(calcRunNumber);				
				finalList.addAll(fileList);
			}
		}
		return psychopyFinalFileMap;
	}

	/**
	 * Gets the run number from sort info.
	 *
	 * @param runSortInfo the run sort info
	 * @param psychopyType the psychopy type
	 * @param sortInfo the sort info
	 * @return the run number from sort info
	 */
	private Integer getRunNumberFromSortInfo(Map<String, List<String>> runSortInfo, String psychopyType, String sortInfo) {
		if (runSortInfo.containsKey(psychopyType)) {
			List<String>infoList = runSortInfo.get(psychopyType);
			// See if we need to add to all run numbers (uploading only run2).
			Integer addVal = 1;
			if (infoList.size()>0 && infoList.get(0)!=null) {
				final String lastChar = String.valueOf(infoList.get(0).charAt(infoList.get(0).length()-1));
				try {
					addVal = Integer.valueOf(lastChar);
				} catch (Exception e) {
					// Do nothing
				}
				if (infoList.contains(sortInfo)) {
					return infoList.indexOf(sortInfo)+addVal;
				}
			}
		}
		return 9;
	}

}
