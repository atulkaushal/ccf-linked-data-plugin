package org.nrg.ccf.linkeddata.importers;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.linkeddata.AbstractLinkedDataImporter;
import org.nrg.ccf.linkeddata.LinkedFileInfo;
import org.nrg.ccf.linkeddata.util.Constants;
import org.nrg.ccf.linkeddata.util.LinkedDataUtil;
import org.nrg.ccf.linkeddata.util.comparators.SessionIdComparator;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;


/**
 * The Class CCF_MDP_LinkedDataImporter.
 * @author Atul
 */
public class CCF_MDP_LinkedDataImporter extends AbstractLinkedDataImporter {

	private static final String TFMRI = "tfMRI";

	/** The logger. */
	static Logger logger = Logger.getLogger(CCF_MDP_LinkedDataImporter.class);

	/** The upload file map. */	
	final Map<File, LinkedFileInfo> uploadFileMap = new HashMap<>();

	/** The session series desc map. */
	private Map<String,Set<String>> sessionSeriesDescMap=new HashMap<String,Set<String>>();

	/** The experiments. */
	List<XnatImagesessiondata> experiments=null;

	/** The subj lbl. */
	String subjLbl=null;

	/** The mdp task mapping prop. */
	static Properties mdpTaskMappingProp=null;

	List<File> leftOverFiles=new ArrayList<File>();

	static
	{

		try {
			InputStream in=CCF_MDP_LinkedDataImporter.class.getResourceAsStream("/mdpTaskDataMapping.properties");
			mdpTaskMappingProp=new Properties();
			mdpTaskMappingProp.load(in);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	} 


	/**
	 * Instantiates a new CCF MDP linked data importer.
	 *
	 * @param listenerControl the listener control
	 * @param u the u
	 * @param fw the fw
	 * @param params the params
	 */
	public CCF_MDP_LinkedDataImporter(Object listenerControl, UserI u, FileWriterWrapperI fw, Map<String, Object> params) {
		super(listenerControl, u, fw, params);
	}


	/**
	 * Instantiates a new CCF MDP linked data importer.
	 *
	 * @param u the u
	 * @param params the params
	 */
	public CCF_MDP_LinkedDataImporter(UserI u, Map<String, Object> params) {
		super(u, params);
	}


	/* (non-Javadoc)
	 * @see org.nrg.ccf.linkeddata.AbstractLinkedDataImporter#processCacheFiles(java.io.File)
	 */
	@Override
	public void processCacheFiles(File cacheLoc) throws ClientException, ServerException {
		Collections.sort(experiments, new SessionIdComparator());

		final List<String> returnList = this.getReturnList();
		returnList.add(MessageFormat.format(Constants.BEGIN_PROCESSING_MESSAGE, subjLbl));
		createSortedScanLists();

		// Start matching EV files to scans
		returnList.add(Constants.PROCESS_SCAN_LEVEL_FILES_MESSAGE);
		File scanLevelDir=new File(new StringBuilder(cacheLoc.getAbsolutePath()).append(File.separator).append("Task Data").toString());
		if(scanLevelDir.exists() && scanLevelDir.isDirectory())
			processScanLevelTaskDataFiles(scanLevelDir);
		else
			returnList.add(Constants.TASK_DATA_DIRECTORY_DOES_NOT_EXISTS);

		// Start matching files to session
		//returnList.add(Constants.PROCESS_SESSION_LEVEL_FILES_MESSAGE);
		// No Session level files as they are uploading session level data as a Dicom file.
		/*File sessionLevelDir=new File(new StringBuilder(cacheLoc.getAbsolutePath()).append(File.separator).append("HCP_MD_physio").toString());
		if(sessionLevelDir.exists() && sessionLevelDir.isDirectory())
			processSessionLevelPsychopyFiles(sessionLevelDir);
		else
			returnList.add(Constants.HCP_MD_PHYSIO_DATA_DIRECTORY_DOES_NOT_EXISTS);*/

		//Boolean upload_status=uploadFilesToResources(uploadFileMap);

		returnList.add("<br><b>REPORT ON LEFTOVER FILES</b>");
		leftOverFiles.addAll(uploadFileMap.keySet());
		LinkedDataUtil.reportLeftoverFiles(leftOverFiles,uploadFileMap,getReturnList());
		
		LinkedDataUtil.addUploadStatus(leftOverFiles.size()==0?false:true, returnList);

		returnList.add(MessageFormat.format(Constants.DATA_UPLOAD_COMPLETED_MESSAGE,subjLbl));
	}




	/* (non-Javadoc)
	 * @see org.nrg.ccf.linkeddata.AbstractLinkedDataImporter#callFromAutomationUploader()
	 */
	@Override
	public List<String> callFromAutomationUploader() throws ClientException, ServerException {
		try {
			if (!this.params.containsKey("BuildPath")) {
				throw new ClientException("ERROR:  BuildPath was not specified");
			}
			verifyAndGetSubjectAndExperiments();

			processCacheFiles(new File(this.params.get("BuildPath").toString()));
			return getReturnList();
		} catch (ClientException e) {
			logger.error("",e);
			getReturnList().add("<br>" + e.toString());
			throw e;
		} catch (ServerException e) {
			logger.error("",e);
			getReturnList().add("<br>" + e.toString());
			throw e;
		} catch (Throwable e) {
			logger.error("",e);
			getReturnList().add("<br>" + e.toString());
			throw new ServerException(e.getMessage(),new Exception());
		}
	}

	/**
	 * Verify and get subject and experiments.
	 *
	 * @throws ClientException the client exception
	 */
	private void verifyAndGetSubjectAndExperiments() throws ClientException {
		if (params.get("subject")==null) {
			clientFailed("ERROR:  Subject parameter (containing subject label) must be supplied for import");
		}
		subjLbl=params.get("subject").toString();
		XnatSubjectdata sub= XnatSubjectdata.getXnatSubjectdatasById(subjLbl, user, true);
		experiments=sub.getExperiments_experiment();
		for (Iterator<XnatImagesessiondata> iterator = experiments.iterator(); iterator.hasNext();) {
			XnatImagesessiondata exp =  iterator.next();
			proj=exp.getProjectData();
		}
	}

	/**
	 * Creates the sorted scan lists.
	 */
	private void createSortedScanLists() {
		for (XnatImagesessiondata exp : experiments) {
			Set<String> seriesDesc = new HashSet<String>();
			for (XnatImagescandataI scan : exp.getScans_scan()) {
				final String[] sdParts = scan.getSeriesDescription().split(Constants.UNDERSCORE);
				if(sdParts!=null && sdParts.length==3 && sdParts[0].startsWith(TFMRI))
				{
					seriesDesc.add(sdParts[1]);
				}
			}
			sessionSeriesDescMap.put(exp.getId(), seriesDesc);
		}
	}

	/**
	 * Process scan level task data files.
	 *
	 * @param taskDir the task dir
	 * @throws ClientException the client exception
	 * @throws ServerException the server exception
	 */
	private void processScanLevelTaskDataFiles(File taskDir) throws ClientException, ServerException {
		File[] subTaskDirs=taskDir.listFiles();
		Arrays.sort(subTaskDirs);
		for (int i = 0; i < subTaskDirs.length; i++) {
			File subTaskDir=subTaskDirs[i];
			if(!mdpTaskMappingProp.keySet().contains(subTaskDir.getName()))
			{
				getReturnList().add("Mapping does not exist for task " +subTaskDir.getAbsolutePath());
				leftOverFiles.addAll(new ArrayList<>(Arrays.asList(subTaskDir.listFiles())));
				continue;
			}
			for (Iterator<String> iterator = sessionSeriesDescMap.keySet().iterator(); iterator.hasNext();) {
				String expLabel =  iterator.next();
				logger.debug("Experiment Label :: "+expLabel);
				if(sessionSeriesDescMap.get(expLabel).contains(mdpTaskMappingProp.get(subTaskDir.getName())))
				{
					XnatImagesessiondata exp1=(XnatImagesessiondata)XnatMrsessiondata.getXnatExperimentdatasById(expLabel, user, true);
					List<XnatImagescandata> scans=exp1.getScans_scan();
					for (Iterator<XnatImagescandata> iterator2 = scans.iterator(); iterator2.hasNext();) {
						XnatImagescandata scan = iterator2.next();

						if (scan.getSeriesDescription().startsWith(TFMRI)) {
							final String[] sdParts = scan.getSeriesDescription().split(Constants.UNDERSCORE);
							if(sdParts[1].equals(mdpTaskMappingProp.get(subTaskDir.getName())) && sdParts.length==3)
							{
								logger.debug("Matched series description :: "+scan.getSeriesDescription());
								File[] taskFiles=subTaskDir.listFiles(new FilenameFilter() {
									public boolean accept(File dir, String name) {
										return !name.startsWith("._");
									}
								});
								for (int j = 0; j < taskFiles.length; j++) {
									logger.debug("File : "+taskFiles[j].getName());
									uploadFileMap.put(taskFiles[j], new LinkedFileInfo(scan, Constants.SESSION_PSYCHOPY_FOLDER));
								}
								if(!uploadFileMap.isEmpty())
								{
									exp=exp1;
									uploadFilesToResources(uploadFileMap);
									uploadFileMap.clear();
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Process session level psychopy files.
	 *
	 * @param psychopySessionsDir the psychopy sessions dir
	 * @throws ClientException the client exception
	 * @throws ServerException the server exception
	 *//*
	private void processSessionLevelPsychopyFiles(File psychopySessionsDir) throws ClientException, ServerException {
		File[] sessionDir=psychopySessionsDir.listFiles();
		for (int i = 0; i < sessionDir.length; i++) {
			int sessionNum=Integer.valueOf(sessionDir[i].getName().split("-")[1].split("_")[0]);
			XnatImagesessiondata exp1=experiments.get(sessionNum-1);
			logger.debug(exp1.getLabel());
			exp=exp1;
			File[] psychopyfiles=sessionDir[i].listFiles();
			for (int j = 0; j < psychopyfiles.length; j++) {
				uploadFileMap.put(psychopyfiles[j], new LinkedFileInfo(exp1,  Constants.SESSION_PSYCHOPY_FOLDER,false));
			}
			uploadFilesToResources(uploadFileMap);
		}
	}*/
}
