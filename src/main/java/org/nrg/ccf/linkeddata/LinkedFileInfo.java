package org.nrg.ccf.linkeddata;

import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;

/**
 * The Class LinkedFileInfo.
 */
public class LinkedFileInfo {
	
	/** The exp. */
	private XnatImagesessiondataI exp;
	
	/** The scan. */
	private XnatImagescandataI scan;
	
	/** The file type. */
	private String fileType;
	
	/** The is scan data. */
	private boolean isScanData;
	
	/**
	 * Instantiates a new linked file info.
	 *
	 * @param scan the scan
	 * @param fileType the file type
	 */
	public LinkedFileInfo(XnatImagescandataI scan, String fileType) {
		super();
		this.scan = scan;
		this.fileType = fileType;
	}
	
	/**
	 * Instantiates a new linked file info.
	 *
	 * @param exp the exp
	 * @param fileType the file type
	 * @param isScanData the is scan data
	 */
	public LinkedFileInfo(XnatImagesessiondataI exp, String fileType, Boolean isScanData) {
		super();
		this.exp = exp;
		this.fileType = fileType;
		this.isScanData=false;
	}
	
	/**
	 * Gets the scan.
	 *
	 * @return the scan
	 */
	public XnatImagescandataI getScan() {
		return scan;
	}
	
	/**
	 * Sets the scan.
	 *
	 * @param scan the new scan
	 */
	public void setScan(XnatImagescandataI scan) {
		this.scan = scan;
	}
	
	/**
	 * Gets the file type.
	 *
	 * @return the file type
	 */
	public String getFileType() {
		return fileType;
	}
	
	/**
	 * Sets the file type.
	 *
	 * @param fileType the new file type
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
	/**
	 * Gets the exp.
	 *
	 * @return the exp
	 */
	public XnatImagesessiondataI getExp() {
		return exp;
	}

	/**
	 * Sets the exp.
	 *
	 * @param exp the new exp
	 */
	public void setExp(XnatImagesessiondataI exp) {
		this.exp = exp;
	}

	/**
	 * Checks if is scan data.
	 *
	 * @return the isScanData
	 */
	public boolean isScanData() {
		return isScanData;
	}

	/**
	 * Sets the scan data.
	 *
	 * @param isScanData the isScanData to set
	 */
	public void setScanData(boolean isScanData) {
		this.isScanData = isScanData;
	}
}
