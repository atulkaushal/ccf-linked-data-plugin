/**
 * 
 */
package org.nrg.ccf.linkeddata.util.comparators;

import java.util.Comparator;

import org.nrg.xdat.model.XnatImagescandataI;


/**
 * The Class ScanTimeComparator.
 *
 * @author Atul
 */
public class ScanTimeComparator implements Comparator<XnatImagescandataI>{

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(final XnatImagescandataI scan1,final XnatImagescandataI scan2) {
		
		final Object scan1time = scan1.getStarttime();
		final Object scan2time = scan2.getStarttime();
		
		// We have some odd physio and secondary scans that don't have a scan time.  We'll just use scanID ordering for these.
		// We won't be placing files in these anyway.
		if (scan1time==null || scan2time==null) {
			return new ScanIdComparator().compare(scan1, scan2);
		}
		
		return ((Comparable)scan1time).compareTo((Comparable)scan2time);
		
	}

}
