/**
 * 
 */
package org.nrg.ccf.linkeddata.util;

import java.io.File;
import java.io.FileFilter;

/**
 * The Class DirectoryFilter.
 *
 * @author Atul
 */
public class DirectoryFilter implements FileFilter{
	
		/* (non-Javadoc)
		 * @see java.io.FileFilter#accept(java.io.File)
		 */
		public boolean accept(File file) {
			return file.isDirectory();
		}
}
