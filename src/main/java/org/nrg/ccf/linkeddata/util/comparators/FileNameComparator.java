/**
 * 
 */
package org.nrg.ccf.linkeddata.util.comparators;

import java.io.File;
import java.io.IOException;
import java.util.Comparator;

/**
 * The Class FileNameComparator.
 *
 * @author Atul
 */
public class FileNameComparator implements Comparator<File>{

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(final File o1,final File o2) {
		try {
			return ((!o1.getName().equals(o2.getName())) ? o1.getName().compareTo(o2.getName()) : o1.getCanonicalPath().compareTo(o2.getCanonicalPath()));
		} catch (IOException e) {
			return ((!o1.getName().equals(o2.getName())) ? o1.getName().compareTo(o2.getName()) : o1.getPath().compareTo(o2.getPath()));
		}
	}

}
