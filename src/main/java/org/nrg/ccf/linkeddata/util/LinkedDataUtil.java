/**
 * 
 */
package org.nrg.ccf.linkeddata.util;

import java.io.File;
import java.sql.Time;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.linkeddata.LinkedFileInfo;
import org.nrg.ccf.linkeddata.util.comparators.FileNameComparator;
import org.nrg.ccf.linkeddata.util.comparators.ScanIdComparator;
import org.nrg.ccf.linkeddata.util.comparators.ScanTimeComparator;
import org.nrg.xdat.model.XnatImagescandataI;

/**
 * The Class LinkedDataUtil.
 *
 * @author Atul
 */
public class LinkedDataUtil {


	
	/**
	 * Sort the scan list.
	 *
	 * @param scanList the scan list
	 * @param returnList the return list
	 */
	public static void sortTheScanList(List<XnatImagescandataI> scanList,List<String> returnList) {

		if(scanList !=null && !scanList.isEmpty())
		{
			// Initially sort by scanId, then see if we can use scanTimeSort
			Collections.sort(scanList, new ScanIdComparator());

			final Object firstScanTimeObj = scanList.get(0).getStarttime();
			final Time firstScanTime;
			if (firstScanTimeObj==null || !(firstScanTimeObj instanceof Time)) {
				returnList.add(Constants.NOTIFY_SCANID_SORT);
				return;
			}
			firstScanTime=(Time)firstScanTimeObj;
			for (XnatImagescandataI scan : scanList) {
				final Object scanTime = scan.getStarttime();
				if (scanTime==null) {
					continue;
				}
				if (!(scanTime instanceof Time) || ((Time)scanTime).before(firstScanTime)) {
					returnList.add(Constants.NOTIFY_SCANID_SORT);
					return;
				}
			}

			Collections.sort(scanList, new ScanTimeComparator());
		}
	}
	
	
	/**
	 * Report leftover files.
	 */
	public static void reportLeftoverFiles(List<File> leftOverFiles,Map<File, LinkedFileInfo> uploadFileMap, List<String> returnList) {
		leftOverFiles=(List<File>) (leftOverFiles==null? uploadFileMap.keySet():leftOverFiles);
		Collections.sort(leftOverFiles, new FileNameComparator());
		for (final File f : leftOverFiles) {
			if (uploadFileMap.get(f) == null) {
				returnList.add("WARNING:  File - <b>" + f.getName() + "</b> - was not uploaded to any scan or to the session.");
			}
		}
	}
	
	public static void addUploadStatus(Boolean upload_status, List<String> returnList)
	{
		if (upload_status) {
			returnList.add(Constants.FILE_UPLOAD_SUCCESS_MESSAGE);
		} else {
			returnList.add(Constants.FILE_UPLOAD_ERROR_MESSAGE);
		}
	}
}
