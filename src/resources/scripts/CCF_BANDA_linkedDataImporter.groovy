import org.nrg.ccf.linkeddata.importers.CCF_BANDA_LinkedDataImporter
import com.google.common.collect.Maps
import java.lang.Exception

Map<String, Object> map = Maps.newHashMap()
map.put("experiment", experiment)
map.put("project", project)
map.put("BuildPath",BuildPath)


def uploader = new CCF_BANDA_LinkedDataImporter(user, map)
try {
     def listout = uploader.call();
     listout.each {
         println it;
     }
     uploader.sendNotifications((user.getEmail() + " akaushal@wustl.edu").split());
} catch (Exception e) {
     println e
     uploader.sendNotifications((user.getEmail() + " akaushal@wustl.edu").split());
}